#! /bin/bash
set -uo pipefail

testWarnOnEmptyConfig() {
    export HOME=$AUTOPKGTEST_TMP
    urlwatch
    result=$?
    assertEquals "On first launch urlwatch should return an exit code 1 due to lack of config file" 1 $result
}

testWithUrlfile(){
    export HOME=$AUTOPKGTEST_TMP
    echo -e "---\nname: \"tmp listing\"\ncommand: \"ls -al $AUTOPKGTEST_TMP\"" > "$AUTOPKGTEST_TMP/urls.json"
    urlwatch --urls "$AUTOPKGTEST_TMP/urls.json"
    result=$?
    assertEquals "Urlwatch should return an exit code 0" 0 $result
}

# load shunit2
. shunit2
